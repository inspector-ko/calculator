import React from "react";

const Operator = props => (
  <div className="num oper" onClick={e => props.changeOperator(e, props.sign)}>
    {props.sign}
  </div>
);

export default Operator;
