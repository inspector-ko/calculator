import React from "react";

const Result = props => (
  <div className="resultat">
    {props.value1}
    {props.operator}
    {props.value2}
  </div>
);

export default Result;
