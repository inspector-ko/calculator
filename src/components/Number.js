import React from "react";

const Number = props => (
  <div className="num" onClick={e => props.addNumber(e, props.value)}>
    {props.value}
  </div>
);

export default Number;
