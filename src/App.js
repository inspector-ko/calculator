import React, { Component } from "react";
import Number from "./components/Number";
import Operator from "./components/Operator";
import Result from "./components/Result";
import "./App.css";

class App extends Component {
  state = {
    result1: "0",
    result2: null,
    currentOperator: null,
    lastCurrentOperator: null,
    isResultShown: false,
    hideResult2: null
    // TODO: only functions can be named as verbs.
    // if it is just varibale with value then it should be noun
  };

  /* Кнопка сброса */
  reset = () => {
    this.setState({
      result1: "0",
      result2: null,
      currentOperator: null,
      lastCurrentOperator: null,
      hideResult2: null,
      isResultShown: null
      // TODO: isResultShown should have boolean value all the time
    });
  };

  /*Определить какой оператор передаётся и вызвать одну из функций ниже */
  getResult = () => {
    // TODO: if you use same state variable more then once then
    // it is better to make an alias like: const currentOperator = this.state.currentOperator
    // or in short notation: const {currentOperator} = this.state;
    // in short notation you can define even many constants like:
    // const {currentOperator, result1, result2} = this.state;
    if (
      (this.state.currentOperator === "/" && this.state.result1 === 0) ||
      (this.state.currentOperator === "/" && this.state.result2 === 0) ||
      (this.state.currentOperator === "x" && this.state.result1 === 0) ||
      (this.state.currentOperator === "x" && this.state.result2 === 0)
    ) {
      this.setState({
        result1: "0",
        result2: null,
        currentOperator: null,
        lastCurrentOperator: null,
        hideResult2: null,
        isResultShown: null
      });
    } else if (this.state.result2 && this.state.result2 !== 0) {
      this.setState({
        hideResult2: this.state.result2,
        result1:
          this.itogo(
            this.state.result1,
            this.state.result2,
            this.state.currentOperator
          ).toFixed(8) * 1,
        // TODO: do not name functions in russian, and function should be a verb, for example getFinalResult
        /*toFixed округляет и ограничивает количество знаков после запятой. умножаем на 1, чтобы убрать лишние нули после исполнения toFixed */ result2: null,
        // TODO: variable (result2 in this case) should not stay after comment, put it on new line
        lastCurrentOperator: this.state.currentOperator,
        currentOperator: null,
        isResultShown: true
      });
      /*результат для выражения c одной переменной и оператором типа 6+ */
    } else if (!this.state.result2 && this.state.currentOperator) {
      this.setState({
        hideResult2: this.state.result1,
        // TODO: you assign result1 to hidden result2? what is the sense?
        result1:
          this.itogo(
            this.state.result1,
            this.state.result1,
            this.state.currentOperator
          ).toFixed(8) * 1,
        // TODO: name functions as verbs
        result2: null,
        lastCurrentOperator: this.state.currentOperator,
        currentOperator: null,
        isResultShown: true
      });
      /*повтор действия последнего оператора с имеющимся результатом */
    } else if (!this.state.result2 && this.state.lastCurrentOperator) {
      this.setState({
        result1:
          this.itogo(
            this.state.result1,
            this.state.hideResult2,
            this.state.lastCurrentOperator
          ).toFixed(8) * 1,
        // TODO: name functions as verbs
        isResultShown: true
      });
    } else if (!this.state.isResultShown && this.state.result1) {
      this.setState({
        result1: this.state.result1,
        result2: null,
        currentOperator: null,
        isResultShown: true
      });
    }
  };
  /*Для замены Eval*/
  itogo = (a, b, operator) => {
    console.log(a, b, operator);
    if (operator === "+") {
      console.log(1);
      return this.sum(a, b, operator);
    } else if (operator === "/") {
      console.log(2);
      return this.divide(a, b, operator);
    } else if (operator === "x") {
      console.log(3);
      return this.multiply(a, b, operator);
    } else if (operator === "-") {
      console.log(4);
      return this.substract(a, b, operator);
    }
  };

  /* itogo2 = () => {
    if (this.state.currentOperator === "+") {
      return this.sum(this.state.result1, this.state.result1);
    } else if (this.state.currentOperator === "/") {
      return this.divide2();
    } else if (this.state.currentOperator === "x") {
      return this.multiply2();
    } else if (this.state.currentOperator === "-") {
      return this.substract2();
    }
  };

  itogo3 = () => {
    if (this.state.lastCurrentOperator === "+") {
      return this.sum3();
    } else if (this.state.lastCurrentOperator === "/") {
      return this.divide3();
    } else if (this.state.lastCurrentOperator === "x") {
      return this.multiply3();
    } else if (this.state.lastCurrentOperator === "-") {
      return this.substract3();
    }
  };*/

  // TODO: why so many functions with subtle differences?
  // better give function an attribute depending on which it will decide what to return
  // alway try to reuse the code, the less copy paste the better

  sum = (a, b) => (+a * 100000000 + +b * 100000000) / 100000000;
  divide = (a, b) => a / b;
  multiply = (a, b) => {
    console.log(a, b);
    return a * b;
  };
  substract = (a, b) => (a * 100000000 - b * 100000000) / 100000000;

  addNumber = (e, number) => {
    //провервка на наличие оператора (введён ли result1 и нажат ли оператор)
    if (this.state.currentOperator) {
      // Если result2 существует и он не равен 0, то
      if (this.state.result2 && this.state.result2 !== "0") {
        //То result2 присваивается старое значение result2 + добавляется новая цифра в конец result2
        return this.setState({ result2: `${this.state.result2}${number}` });
      }

      return this.setState({ result2: number });
    }

    if (!this.state.isResultShown && this.state.result1 !== "0") {
      return this.setState({ result1: `${this.state.result1}${number}` });
    }
    //Если результат показан, то сбрасываем результат и меняем на новое значение при нажатии на цифру
    return this.setState({ isResultShown: false, result1: number });
  };
  /*Меняет оператор после result1 на другой */
  //проверить существует ли result2, то запустить getresult
  changeOperator = (e, operator) => {
    if (this.state.result2) {
      this.getResult();
    }
    return this.setState({ currentOperator: operator });
  };

  render() {
    return (
      <div className="calculator">
        <div className="pole">
          <div className="bill">
            <Result
              value1={this.state.result1}
              value2={this.state.result2}
              operator={
                this.state.currentOperator ? this.state.currentOperator : ""
              }
            />
          </div>
          <div className="num but">mrc</div>
          <div className="num but">m-</div>
          <div className="num but">m+</div>
          <Operator sign={"/"} changeOperator={this.changeOperator} />
          <Number value={7} addNumber={this.addNumber} />
          <Number value={8} addNumber={this.addNumber} />
          <Number value={9} addNumber={this.addNumber} />
          <Operator sign={"x"} changeOperator={this.changeOperator} />
          <Number value={4} addNumber={this.addNumber} />
          <Number value={5} addNumber={this.addNumber} />
          <Number value={6} addNumber={this.addNumber} />
          <Operator sign={"-"} changeOperator={this.changeOperator} />
          <Number value={1} addNumber={this.addNumber} />
          <Number value={2} addNumber={this.addNumber} />
          <Number value={3} addNumber={this.addNumber} />
          <Operator sign={"+"} changeOperator={this.changeOperator} />
          <Number value={0} addNumber={this.addNumber} />
          <Number value={`.`} addNumber={this.addNumber} />
          <div className="num" onClick={this.reset}>
            C
          </div>
          <div className="num rovno" onClick={this.getResult}>
            =
          </div>
        </div>
      </div>
    );
  }
}

export default App;
